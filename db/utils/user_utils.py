from typing import Literal

from sqlalchemy import inspect
from sqlalchemy.orm import Session
from mixer.backend.sqlalchemy import Mixer

from db.models import user
from db import db_setup
from pydantic_models import users_pydantic


def get_all_users_of_db() -> list[user.UserAlchemy]:
    """
    Обращение к БД, получение всех пользователей со всеми столбцами.
    """
    session = db_setup.GetSession()
    try:
        query = session.query(user.UserAlchemy)
        return query.all()
    finally:
        session.close()


def get_users_with_filtering(gender: Literal['male', 'female'] | None = None, limit: int = 100) -> list[
    user.UserAlchemy]:
    """
    Обращение к БД, получение пользователей с возможностью фильтрации по полу, и установленным лимитом.
    """
    session = db_setup.GetSession()
    try:
        query = session.query(user.UserAlchemy)
        if gender:
            query = query.filter_by(gender=gender)
        query = query.limit(limit=limit)
        response = query.all()
        return response
    finally:
        session.close()


def save_user_to_db(user_pydantic: users_pydantic.UserInputPydantic) -> user.UserAlchemy:
    """
     Обращение к БД, создание новой записи-пользователя.
    """
    session = db_setup.GetSession()
    new_user = user.UserAlchemy(id=user_pydantic.id, name=user_pydantic.name, gender=user_pydantic.gender)
    try:
        session.add(new_user)
        session.commit()
        session.refresh(instance=new_user)
        return new_user
    finally:
        session.close()


def check_exist_user(id_: int | None = None) -> bool:
    """
     Обращение к БД, проверка на существование пользователя с таким id.
     Или просто на существование хоть одной записи.
    """
    session = db_setup.GetSession()
    q = session.query(user.UserAlchemy.id)
    if id_:
        q = q.filter(user.UserAlchemy.id == id_)

    if session.query(q.exists()).scalar():
        return True
    return False


def table_exists(name: str, engine_: db_setup.engine = db_setup.engine) -> bool:
    inspect_ = inspect(subject=engine_)
    result = inspect_.dialect.has_table(db_setup.engine.connect(), name)
    return result


def blend_test_data(session: Session = db_setup.GetSession(), quantity: int = 100, for_table: str = 'user') -> None:
    if not table_exists(name=for_table) or check_exist_user():
        return

    mixer = Mixer(session=session, commit=True)
    mixer.cycle(count=quantity).blend(user.UserAlchemy, gender=mixer.RANDOM('male', 'female'))
