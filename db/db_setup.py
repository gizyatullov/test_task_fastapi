from os import getenv

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database

SQLALCHEMY_DB_URL = f'postgresql+psycopg2://{getenv("DB_USER")}:{getenv("DB_PASSWORD")}@{getenv("DB_IP")}:' \
                    f'{getenv("DB_PORT")}/{getenv("DB_NAME")}'

engine = create_engine(url=SQLALCHEMY_DB_URL, future=True)

if not database_exists(url=engine.url):
    create_database(url=engine.url)

GetSession = sessionmaker(autocommit=False, autoflush=False, bind=engine, future=True)

Base = declarative_base()
