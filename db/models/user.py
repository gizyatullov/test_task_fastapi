from sqlalchemy import Column, Integer, String

from .. import db_setup


class UserAlchemy(db_setup.Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(50), nullable=False)
    gender = Column(String(50), nullable=False)
