from typing import Literal

from pydantic import BaseModel, validator

from db.utils import user_utils


class UserOutputPydantic(BaseModel):
    id: int
    name: str
    gender: Literal['male', 'female']

    class Config:
        orm_mode = True

    @validator('name')
    def name_max_min_length(cls, value):
        if len(value) > 50:
            raise ValueError('the length should not be more than 50')
        if len(value) < 3:
            raise ValueError('the length must be at least 3')
        return value


class UserInputPydantic(UserOutputPydantic):

    @validator('id')
    def check_exist(cls, value):
        if user_utils.check_exist_user(id_=value):
            raise ValueError('an entry with this id already exists')
        return value
