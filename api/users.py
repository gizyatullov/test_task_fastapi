from typing import Literal

from fastapi import APIRouter

from pydantic_models import users_pydantic
from db.utils import user_utils

router = APIRouter()


@router.get(path='/get_all_users', response_model=list[users_pydantic.UserOutputPydantic])
def get_all_users():
    users = user_utils.get_all_users_of_db()
    return users


@router.get(path='/get_users', response_model=list[users_pydantic.UserOutputPydantic])
def get_users(gender: Literal['male', 'female'] | None = None, limit: int = 100):
    users = user_utils.get_users_with_filtering(gender=gender, limit=limit)
    print(users)
    return users


@router.post(path='/user', response_model=users_pydantic.UserOutputPydantic, status_code=201)
def create_user(user: users_pydantic.UserInputPydantic):
    user_in_db = user_utils.save_user_to_db(user_pydantic=user)
    return user_in_db
