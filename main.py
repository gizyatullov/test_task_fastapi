from os import getenv

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from dotenv import load_dotenv

load_dotenv()

from db.models import user
from db.utils import user_utils
from api import users

user.db_setup.Base.metadata.create_all(bind=user.db_setup.engine)
user_utils.blend_test_data()

app = FastAPI(
    title='FastAPI test task',
    version='0.0.1',
    contact={
        'telegram': '@EricGizyatullov',
    },
    license_info={
        'name': 'MIT',
    }
)

origins = [
    'http://127.0.0.1:5194',
    'http://localhost:5194',
]

app.add_middleware(
    middleware_class=CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

app.include_router(users.router)

# if __name__ == '__main__':
#     uvicorn.run(app='main:app',
#                 reload=True,
#                 host=getenv('UVICORN_IP'),
#                 port=int(getenv('UVICORN_PORT')))
