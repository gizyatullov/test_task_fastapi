FROM python:3.10.6
RUN pip install --upgrade pip
WORKDIR /app
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5194
#CMD ["python", "main.py"]